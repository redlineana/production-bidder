import path from 'path'
import Express from 'express'
import React from 'react'
import {createStore} from 'redux'
import {Provider} from 'react-redux'
import App from './src/App'
import { RoutingContext,match } from 'react-router';
import {renderToString} from 'react-dom/server'
import configureStore from './src/store/configureStore'
import initialState from './src/store/initialState'

/*
 import counterApp from './reducers'
 import App from './containers/App'*/

const tracer = require('signalfx-tracing').init({
    // Service name, also configurable via
    // SIGNALFX_SERVICE_NAME environment variable
    service: 'auction-app',
    // Smart Agent or Gateway endpoint, also configurable via
    // SIGNALFX_ENDPOINT_URL environment variable
    url: 'http://192.168.198.15:9080/v1/trace', // http://localhost:9080/v1/trace by default
    // Optional organization access token, also configurable via
    // SIGNALFX_ACCESS_TOKEN environment variable
    accessToken: 'imwVjw2Dk2_quTThqQGtSQ',
    // Optional environment tag
    tags: {environment: 'Production-deger'}
  })




  module.exports = tracer


const app = Express();

const port = 3000;

const handleRender = (req, res)=> {
    const store = configureStore(initialState(),true);
   /* const initialView = (
        <Provider store={store}>
            <Router history={browserHistory}>
                <Route path="/" component={App} />
            </Router>
        </Provider>
    )*/
    const html = renderToString(
        <Provider store={store}>
            <App/>
        </Provider>
    )
    console.log('heheh he');
    res.send(renderFullPage(html));
}




const renderFullPage = (html)=> {
    return `
    <!doctype html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" href="/favicon.ico">
            <link rel="stylesheet" href="/font-awesome/css/font-awesome.min.css">
            <title>Buttercup Store Bidding</title>
        </head>
        <body>
            <div id="root">${html}</div>
        </body>
    </html>`;
}


const tracer = require('signalfx-tracing').init()
const formats = require('signalfx-tracing/ext/formats');

class Logger {
    log(level, message) {
        const span = tracer.scope().active();
        const time = new Date().toISOString();
        const record = { time, level, message };

        if (span) {
            tracer.inject(span.context(), formats.LOG, record);
        }

        console.log(JSON.stringify(record));
    }
}

module.exports = Logger;


app.use('/', express.static(path.join(__dirname, '/public')));
app.use(handleRender);

app.listen(port);
